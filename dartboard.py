from turtle import *

# note that I use the noinspection PyUnResolvedReferences to remove
# an error pycharm gives importing from turtle.
# bug report at: https://youtrack.jetbrains.com/issue/PY-9658
# It simply stops pycharm from displaying a bunch of errors to me while I program

SECTION_COUNT = 20

# the numbers to draw around the border put in clockwise rotation
NUMBERS = ["20", " 1", "18", " 4", "13", " 6", "10", "15", " 2", "17",
           " 3", "19", " 7", "16", " 8", "11", "14", " 9", "12", " 5"]

DOUBLE_TRIPLE_THICKNESS = 20
# the point from the outside of the board to draw the triples
TRIPLE_START = 140
NUMBER_PADDING = 40


# noinspection PyUnresolvedReferences
def initialize_turtle():
    """
    Initialize the turtle settings
    """
    shape("turtle")
    pencolor("gray")
    pensize(1)
    speed(0)


# noinspection PyUnresolvedReferences
def get_double_triple_colour(index):
    """
    Function to get the colour required for the "double" and "triple" pieces
    :param index: the index of the current piece
    :return: The colour to draw the section
    """
    return "green" if index % 2 == 0 else "red"


# noinspection PyUnresolvedReferences
def draw_border(radius):
    """
    Draws the outline border for the numbers on the dartboard
    :param radius: The radius of the inside dartboard
    """
    penup()
    sety(-radius - NUMBER_PADDING)
    pendown()

    fillcolor("black")
    begin_fill()
    circle(radius + NUMBER_PADDING)
    end_fill()


# noinspection PyUnresolvedReferences
def draw_fat_and_small_pie(index, radius):
    """
    Draws the triangular parts of the dart board
    :param index: The index of the piece to draw
    :param radius: The radius of the inside of the dart board
    """
    # no need to draw the black parts!
    if index % 2 != 0:
        return

    fillcolor("white")
    begin_fill()
    circle(radius, 18)
    left(90)
    forward(radius)
    left(162)
    forward(radius)
    end_fill()
    left(90)


# noinspection PyUnresolvedReferences
def draw_double_triple(index, radius):
    """
    Draws a double or triple sections of the board
    :param index: The index of the piece to draw
    :param radius: The radius of the inside of the dart board
    """
    fillcolor(get_double_triple_colour(index))
    begin_fill()
    circle(radius, 18)
    left(90)
    forward(DOUBLE_TRIPLE_THICKNESS)
    left(90)
    circle(-radius + DOUBLE_TRIPLE_THICKNESS, 18)
    left(90)
    forward(DOUBLE_TRIPLE_THICKNESS)
    left(90)
    end_fill()
    # move us into position for the next piece to be drawn
    circle(radius, 18)


# noinspection PyUnresolvedReferences
def draw_bullseye(color, width):
    """
    Draws a bullseye on the dart board
    :param color: The colour of the bullseye to draw
    :param width: The width of the bullseye to draw
    """
    penup()
    home()
    begin_fill()
    fillcolor(color)
    sety(-width)
    pendown()
    circle(width)
    end_fill()


# noinspection PyUnresolvedReferences
def draw_numbers(radius):
    """
    Draws the numbers around the board
    :param radius: The radius of the inside of the board
    """
    penup()
    sety(radius)
    # move slightly to the left to make the numbers start in the centre
    setx(-15)
    pendown()
    for i in range(SECTION_COUNT):
        write(NUMBERS[i], move=False, align="left", font=("Arial", 20, "normal"))
        penup()
        circle(-radius - NUMBER_PADDING / 2, 18)
        pendown()


# noinspection PyUnresolvedReferences
def draw_dartboard(radius):
    """
    Draws the dartboard to the screen
    :param radius: The radius of the inside of the dartboard
    """
    draw_border(radius)
    draw_numbers(radius)

    # start -BOARD_RADIUS pixels below the centre of the screen
    penup()
    setx(0)
    sety(-radius)

    # move half a section over so we align properly
    circle(radius, 9)
    pendown()

    for i in range(SECTION_COUNT):
        draw_fat_and_small_pie(i, radius)
        draw_double_triple(i, radius)

    penup()
    home()
    sety(-radius + TRIPLE_START)

    # move half a section over so we align properly
    circle(radius - TRIPLE_START, 9)
    pendown()

    for i in range(SECTION_COUNT):
        draw_double_triple(i, radius - TRIPLE_START)

    draw_bullseye("green", 20)
    draw_bullseye("red", 10)

    penup()
    home()
    pendown()


# noinspection PyUnresolvedReferences
def main():
    board_radius = 285
    initialize_turtle()
    draw_dartboard(board_radius)
    hideturtle()
    exitonclick()

main()
